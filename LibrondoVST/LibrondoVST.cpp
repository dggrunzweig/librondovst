//
//  LibrondoVST.cpp
//  LibrondoVST
//
//  Created by David Grunzweig on 1/14/16.
//  Copyright © 2016 David Grunzweig. All rights reserved.
//

#include "LibrondoVST.hpp"

//sets default values when the program is started
LibrondoVSTProgram::LibrondoVSTProgram()
{
    fDelay = 0.5;
    fFeedBack = 0.5;
    fOut = 0.75;
    
    strcpy (name, "Init");
}


LibrondoVST::LibrondoVST (audioMasterCallback audioMaster) : AudioEffectX (audioMaster, kNumPrograms, kNumParams)
{
    // init
    size = 44100;
    buffer = new float[size];
    
    programs = new LibrondoVSTProgram[numPrograms];
    
    if (programs)
        setProgram (0);
    
    setNumInputs (2);	// mono input
    setNumOutputs (2);	// stereo output
    canProcessReplacing ();
    setUniqueID ('LVST');
    
}

LibrondoVST::~LibrondoVST ()
{
    // nothing to do here
}

void LibrondoVST::setProgram (int program)
{
    LibrondoVSTProgram * ap = &programs[program];
    
    curProgram = program;
    setParameter (kDelay, ap->fDelay);
    setParameter (kFeedBack, ap->fFeedBack);
    setParameter (kOut, ap->fOut);
}

void LibrondoVST::setProgramName (char *name)
{
    strcpy (programs[curProgram].name, name);
}

void LibrondoVST::getProgramName (char *name)
{
    if (!strcmp (programs[curProgram].name, "Init"))
        sprintf (name, "%s %d", programs[curProgram].name, curProgram + 1);
    else
        strcpy (name, programs[curProgram].name);
}

//-----------------------------------------------------------------------------------------
void LibrondoVST::setParameter (int index, float value)
{
    LibrondoVSTProgram * ap = &programs[curProgram];
    
    switch (index)
    {
        case kDelay :    fDelay = ap->fDelay = value; break;
        case kFeedBack : fFeedBack = ap->fFeedBack = value; break;
        case kOut :      fOut = ap->fOut = value; break;
    }
//    if (editor)
//        editor->postUpdate ();
}

//-----------------------------------------------------------------------------------------
float LibrondoVST::getParameter (int index)
{
    float v = 0;
    
    switch (index)
    {
        case kDelay :    v = fDelay; break;
        case kFeedBack : v = fFeedBack; break;
        case kOut :      v = fOut; break;
    }
    return v;
}

void LibrondoVST::getParameterName (int index, char *label)
{
    switch (index)
    {
        case kDelay :    strcpy (label, "Delay"); break;
        case kFeedBack : strcpy (label, "FeedBack"); break;
        case kOut :      strcpy (label, "Volume"); break;
    }
}

//------------------------------------------------------------------------
void LibrondoVST::getParameterDisplay (int index, char *text)
{
    switch (index)
    {
        case kDelay :    AudioEffectX::int2string (delay, text, 4); break;
        case kFeedBack : float2string (fFeedBack, text, 3);	break;
        case kOut :      dB2string (fOut, text, 4); break;
    }
}

//------------------------------------------------------------------------
void LibrondoVST::getParameterLabel (int index, char *label)
{
    switch (index)
    {
        case kDelay :    strcpy (label, "samples");	break;
        case kFeedBack : strcpy (label, "amount");	break;
        case kOut :      strcpy (label, "dB");	break;
    }
}

//------------------------------------------------------------------------
bool LibrondoVST::getEffectName (char* name)
{
    strcpy (name, "LibrondoVST");
    return true;
}

//------------------------------------------------------------------------
bool LibrondoVST::getProductString (char* text)
{
    strcpy (text, "LibrondoVST");
    return true;
}

//------------------------------------------------------------------------
bool LibrondoVST::getVendorString (char* text)
{
    strcpy (text, "Steinberg Media Technologies");
    return true;
}

//-----------------------------------------------------------------------------------------
void LibrondoVST::process (float **inputs, float **outputs, int sampleFrames)
{
    float *in1  =  inputs[0];
    float *in2  =  inputs[1];
    float *out1 = outputs[0];
    float *out2 = outputs[1];
    //in1 left input buffer, in2 right input buffer
}

//-----------------------------------------------------------------------------------------
void LibrondoVST::processReplacing (float **inputs, float **outputs, int sampleFrames)
{
    float *in1  =  inputs[0];
    float *in2  =  inputs[1];
    float *out1 = outputs[0];
    float *out2 = outputs[1];
    //in1 left input buffer, in2 right input buffer
 
}
