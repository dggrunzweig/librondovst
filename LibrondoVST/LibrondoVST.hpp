//
//  LibrondoVST.h
//  LibrondoVST
//
//  Created by David Grunzweig on 1/14/16.
//  Copyright © 2016 David Grunzweig. All rights reserved.
//

#ifndef LibrondoVST_hpp
#define LibrondoVST_hpp

#include "audioeffectx.h"
#include <stdio.h>

enum
{
    // Global
    kNumPrograms = 16,
    
    // Parameters Tags
    kDelay = 0,
    kFeedBack,
    kOut,
    
    kNumParams
};

class LibrondoVST;

//------------------------------------------------------------------------
class LibrondoVSTProgram
{
    friend class LibrondoVST;
public:
    LibrondoVSTProgram();
    ~LibrondoVSTProgram() {}
    
private:
    float fDelay;
    float fFeedBack;
    float fOut;
    char name[24];
};

//------------------------------------------------------------------------
class LibrondoVST: public AudioEffectX
{
public:
    LibrondoVST (audioMasterCallback audioMaster);
    ~LibrondoVST ();
    
    virtual void process (float **inputs, float **outputs, int sampleframes);
    virtual void processReplacing (float **inputs, float **outputs, int sampleFrames);
    
    virtual void setProgram (int program);
    virtual void setProgramName (char *name);
    virtual void getProgramName (char *name);
    
    virtual void setParameter (int index, float value);
    virtual float getParameter (int index);
    virtual void getParameterLabel (int index, char *label);
    virtual void getParameterDisplay (int index, char *text);
    virtual void getParameterName (int index, char *text);
    
    virtual bool getEffectName (char* name);
    virtual bool getVendorString (char* text);
    virtual bool getProductString (char* text);
    virtual int getVendorVersion () { return 1000; }
    virtual VstPlugCategory getPlugCategory () { return kPlugCategEffect; }
    
protected:
    void setDelay (float delay);
    LibrondoVSTProgram *programs;
    
    float fDelay, fFeedBack, fOut;
    
    int delay;
    int size;
    int cursor;
};

#endif /* LibrondoVST_hpp */
