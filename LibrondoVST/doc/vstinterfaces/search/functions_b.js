var searchData=
[
  ['max',['Max',['../../base/namespaceSteinberg.html#a0f480f7384f085c2606eff5f310ad194',1,'Steinberg']]],
  ['memorystream',['MemoryStream',['../../vstsdk/classSteinberg_1_1MemoryStream.html#a28d4c334557e9abb58b418e80c709905',1,'Steinberg::MemoryStream::MemoryStream()'],['../../vstsdk/classSteinberg_1_1MemoryStream.html#a6bcbcbec3385623cdb252d1082ae5766',1,'Steinberg::MemoryStream::MemoryStream(void *memory, TSize memorySize)']]],
  ['min',['Min',['../../base/namespaceSteinberg.html#affba9ed0dfe13309d79094f10bbc6021',1,'Steinberg']]]
];
